#!/bin/bash

TF_BOLD=$(tput bold)
TF_RESET=$(tput sgr0)

if [ -f "secrets.sh" ]; then
	source secrets.sh
else
	echo "${TF_BOLD}AVISO:${TF_RESET} considere criar o arquivo secrets.sh baseado no secrets.example.sh"
fi
if [ -z "$DOCKER_IMG_NAME" ]; then
	DOCKER_IMG_NAME=monitoria
fi
if [ -z "$DOCKER_CONTAINER_NAME" ]; then
	DOCKER_CONTAINER_NAME=monitoria
fi
if [ -z "$DEV_PORT" ]; then
	DEV_PORT=3000
fi
if [ -z "$LOCAL_PORT" ]; then
	LOCAL_PORT=3000
fi
if [ -z "$REPO_DIR" ]; then
	REPO_DIR="$(readlink -f "$0")"
	REPO_DIR="$(dirname "${REPO_DIR}")"
fi
CONTIANER_REPO_DIR="/root/monitoria-imeusp"


help() {
	echo "Comandos disponíveis: (executar FORA do container)"
	echo "${TF_BOLD}build-img${TF_RESET}"
	echo "    Constrói a imgem de acordo com Dockerfile"
	echo "${TF_BOLD}start${TF_RESET}"
	echo "    Inicia um container em segundo plano"
	echo "${TF_BOLD}start-console${TF_RESET}"
	echo "    Inicia um container e abre o console"
	echo "${TF_BOLD}stop${TF_RESET}"
	echo "    Pára um container em segundo plano" #Acho a grafia antiga menos ambígua aqui
	echo "${TF_BOLD}list${TF_RESET}"
	echo "    Lista os containers em execução"
	echo "${TF_BOLD}console${TF_RESET}"
	echo "    Abre o console dentro do container"
	echo "${TF_BOLD}exec-cmd${TF_RESET}"
	echo "    Executa um comando com a saída padrão atual"
	echo "${TF_BOLD}send-cmd${TF_RESET}"
	echo "    Executa um comando com em plano de fundo"
	echo "${TF_BOLD}bundle-install${TF_RESET}"
	echo "    Instala as gems"
	echo "${TF_BOLD}rails-start${TF_RESET}"
	echo "    Inicia a aplicação web da monitoria"
	echo "${TF_BOLD}tunnel${TF_RESET}"
	echo "    Abre um túnel de SSH ao servidor de desenvolvimento"
	echo "${TF_BOLD}tunnel-ssh${TF_RESET}"
	echo "    Abre uma sessão e um túnel de SSH ao servidor de desenvolvimento"
	echo "${TF_BOLD}ssh${TF_RESET}"
	echo "    Abre uma sessão de SSH ao servidor de desenvolvimento"
	echo ""
	echo "Todos os comandos são feitos para serem executados fora do container"
	echo ""
	echo "Por padrão o nome do container a ser usado é monitoria, mas ele pode ser alterado pela"
	echo "variável de ambiente DOCKER_CONTAINER_NAME (ex: export DOCKER_CONTAINER_NAME=\"nome2\")"
}

build-img() {
	echo "sudo docker build -t ${DOCKER_IMG_NAME} ."
	sudo docker build -t ${DOCKER_IMG_NAME} .
}

start-container() {
	CMD="sudo docker run -dt --rm -v ${REPO_DIR}:${CONTIANER_REPO_DIR} -p 3000:3000 --name ${DOCKER_CONTAINER_NAME} ${DOCKER_IMG_NAME} /bin/bash"
	echo $CMD
	eval $CMD
}

start-console() {
	start-container;
	console;
}

stop-container() {
	CMD="sudo docker stop ${DOCKER_CONTAINER_NAME}"
	echo $CMD
	eval $CMD
}

list-containers() {
	CMD="sudo docker ps -a"
	echo $CMD
	eval $CMD
}

exec-cmd() {
	CMD="sudo docker exec -it ${DOCKER_CONTAINER_NAME} ${@}"
	echo $CMD
	eval $CMD
}

send-cmd() {
	CMD="sudo docker exec -t ${DOCKER_CONTAINER_NAME} ${@}"
	echo $CMD
	eval $CMD
}

run-ssh() {
	CMD="ssh ${IMEUSP_USER}@${IMEUSP_HOST} -p ${IMEUSP_PORT}"
	echo $CMD
	eval $CMD
}

tunnel() {
	CMD="ssh -fN -L ${LOCAL_PORT}:localhost:${DEV_PORT} ${IMEUSP_USER}@${IMEUSP_HOST} -p ${IMEUSP_PORT}"
	echo $CMD
	eval $CMD
}

tunnel-ssh() {
	CMD="ssh -L ${LOCAL_PORT}:localhost:${DEV_PORT} ${IMEUSP_USER}@${IMEUSP_HOST} -p ${IMEUSP_PORT}"
	echo $CMD
	eval $CMD
}

bundle-install() {
	CMD="${CONTIANER_REPO_DIR}/helper_internal.sh bundle-install"
	exec-cmd $CMD
}

rails-start() {
	CMD="${CONTIANER_REPO_DIR}/helper_internal.sh rails-start"
	exec-cmd $CMD
}

console() {
	CMD="/bin/bash"
	exec-cmd $CMD
}

case "$1" in
	"build-img" )
		build-img ${@:2};;
	"start" )
		start-container ${@:2};;
	"start-console" )
		start-console ${@:2};;
	"stop" )
		stop-container ${@:2};;
	"list" )
		list-containers ${@:2};;
	"console" )
		console ${@:2};;
	"exec-cmd" )
		exec-cmd ${@:2};;
	"send-cmd" )
		send-cmd ${@:2};;
	"tunnel" )
		tunnel ${@:2};;
	"tunnel-ssh" )
		tunnel-ssh ${@:2};;
	"ssh" )
		run-ssh ${@:2};;
	"bundle-install" )
		bundle-install ${@:2};;
	"rails-start" )
		rails-start ${@:2};;
	*)
		help;;
esac
