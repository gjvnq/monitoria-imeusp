#!/bin/bash

if [ -z "$RAILS_ENV" ]; then
	RAILS_ENV="development"
fi
if [ -z "$REPO_DIR" ]; then
	REPO_DIR="$(readlink -f "$0")"
	REPO_DIR="$(dirname "${REPO_DIR}")"
fi


TF_BOLD=$(tput bold)
TF_RESET=$(tput sgr0)

help() {
	echo "Comandos disponíveis: (executar DENTRO do container)"
	echo "${TF_BOLD}bundle-install${TF_RESET}"
	echo "    Instala as gems"
	echo "${TF_BOLD}rails-start${TF_RESET}"
	echo "    Inicia a aplicação web da monitoria"
	echo "${TF_BOLD}rake${TF_RESET}"
	echo "    Prepara o banco de dados"
}

bundle-install() {
	CMD="bundle install --path vendor/bundle"
	echo $CMD
	eval $CMD
}

rails-start() {
	CMD="bin/rails server"
	echo $CMD
	eval $CMD
}

rake-run() {
	CMD="rake db:setup"
	echo $CMD
	eval $CMD
	CMD="rake db:migrate RAILS_ENV=${RAILS_ENV}"
	echo $CMD
	eval $CMD
}

cd ${REPO_DIR}

case "$1" in
	"bundle-install" )
		bundle-install ${@:2};;
	"rails-start" )
		rails-start ${@:2};;
	"rake" )
		rake-run ${@:2};;
	*)
		help;;
esac
