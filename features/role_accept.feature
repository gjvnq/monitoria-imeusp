Feature: Assistant roles confirmation
    In order to become an assistant or not
    As a candidate student
    I want to accept or reject my assistant roles

    Background:
        When there is an open semester "2014" "1"
        And there is a department with code "MAC"
        And there is a course with name "Introdução à Ciência da Computação" and code "MAC0110" and department "MAC"
        And there is a student with name "Bob" with nusp "123456" and email "aluno@usp.br"
        And there is a professor with name "Dude" and nusp "111111" and department "MAC" and email "prof@ime.usp.br"
        And there is a request for teaching assistant by professor "Dude" for the course "MAC0110"
        And there is a candidature by student "Bob" for course "MAC0110"
        And there is a confirmed assistant role for student "Bob" with professor "Dude" at course "MAC0110"

    Scenario: Student accepts assistant role
        Given I'm logged in as student "Bob"
        And I'm at the home page
        And I click the "Minhas candidaturas" link
        And I should see "Você foi eleito para ser monitor em uma ou mais disciplinas"
        When I click the "Aceitar" link
        Then I should see "Presenças marcadas"
        And I should not see "Você não tem nenhuma monitoria"

    Scenario: Student rejects assistant role
        Given I'm logged in as student "Bob"
        And I'm at the home page
        And I click the "Minhas candidaturas" link
        And I should see "Você foi eleito para ser monitor em uma ou mais disciplinas"
        When I click the "Recusar" link
        Then I should not see "Presenças marcadas"
        And I should see "Você não tem nenhuma monitoria"
