# Sobre o repositório

O Sistema de Monitoria é o sistema de pedidos e candidaturas de monitores para disciplinas do Instituto de Matemática e Estatística da USP.]

Ele foi desenvolvido durante a disciplina **MAC 342/5716 (Laboratório de Programação eXtrema)**.

# Instalação

## Clonando o repositório

Para clonar o repositório, entre em um diretório de preferência e em seguida execute (lembre-se que ao clonar, os arquivo do repositório serão copiado para uma pasta chamada *monitoria-imeusp*):

```bash
git clone https://github.com/monitoria-imeusp/monitoria-imeusp
```

Ou, se preferir por ```ssh```, execute:

```bash
git clone git@github.com:monitoria-imeusp/monitoria-imeusp.git
```

**Note**: para configurar uma chave ```ssh```, siga este [tutorial](https://help.github.com/articles/generating-an-ssh-key/).

## Docker
Utiliza o Dockerfile disponibilizado no root. Para que a imagem possa ser perfeitamente utilizada em seu computador, verifique os [requisitos de utilização do Docker Community Edition](https://docs.docker.com/engine/installation/)

### Build
Rode o seguinte comando na pasta do projeto:
- ```./helper_external.sh build-img```
- Pode demorar um pouco, mas a criação da imagem acontecerá com sucesso, ou deveria, caso contrário [nos contate](https://gitlab.com/monitoria-imeusp/monitoria-imeusp/settings/members)

### Run
Iniciamos o ambiente com o comando: ```./helper_external.sh start-console```
- O terminal entrará como root na pasta /root
- ```ls -l``` deverá ser suficiente para ver a pasta "monitoria" que está no seu pc e pode ser acessada pelo terminal da máquina virtual. É como se fosse um atalho da máquina para seu pc, então pode usar seu editor de texto predileto sem problemas direto do seu computador mesmo.
- Na sua máquina (host), dentro da pasta do projeto, dê o seguinte comando: ```sudo chown -R (seu_usuario) .```*


### Preparar o ambiente:
- ```cd monitoria-imeusp```
- ```./helper_internal.sh bundle-install```
- ```./helper_internal.sh rake```

### Testes:
- `rspec`
- `bundle exec cucumber HISTORY_REQUEST_URL=http://abc/`

## Breve explicação sobre as bandeiras do Docker

O comando ```./helper_external.sh start-console``` na verdade executa ```sudo docker run -it --rm -v (REPO):/root/monitoria-imeusp -p 3000:3000 --name (CONTIANER_NAME) (IMAGE_NAME)``` onde `(REPO)` é o caminho repositório atual.

`-it`: significa de forma "iterativa", na verdade são duas flags, mas para termos o pseudo-terminal conversando com a gente, precisamos dessa coisa linda, aceite.

`--rm`: significa que o estado da máquina não será preservado, ou seja, você pode dar `apt-get install badoo` ou o que mais você quiser fazer com a pobre coitada da imagem que não ficará salvo. "Mas, admin, e as alterações no código?" Veja a flag abaixo. Elas ficam [salvas, porque estão no seu computador](https://pt.wikipedia.org/wiki/Persist%C3%AAncia_(ci%C3%AAncia_da_computa%C3%A7%C3%A3o)), agora as configurações da máquina se mantém aquelas que foram "buildadas".

`-v`: é o atalho pc -> máquina. Qualquer coisa que você colocar do lado esquerdo (esquerdo:direito) ficará disponível no caminho direito dentro da VM. Se o caminho não existir, por exemplo, direito= "/pedra/papel/tesoura/chamito/batom", ele vai criar esse caminho para você automaticamente, porque o Docker é inteligente assim.

`-p`: libera as portas para que você e o mundo possa escutar. É como construir uma porta numa casa mesmo. Imagina que sem isso, o Docker está numa prisão. Você pode usar só "-P", mas aí a atribuição das portas fica automática. "E como eu vejo em que porta está assim?" `sudo docker ps -a` deve ficar descrito, geralmente é 30000 e alguma coisa.
Sim, é possível usar as flag 'p' e 'v' mais de uma vez para criar seu monstro favorito.

### Comandos fundamentais (todos começam com `sudo docker`)
##### (Utilize o ID das imagens e das instâncias que são obtidas quando listadas)
`ps -a` : lista todas as instâncias

`ps stop $(sudo docker ps -aq)` : para todas as instâncias

`ps rm $(sudo docker ps -aq)` : remove todas as instâncias

`images` : lista todas as imagens

`rmi (imagem)` : remove dada imagem

### Dicas

Use ```./helper_external.sh help``` para ver as opções disponíveis.

[Clique em mim para mais informações úteis e complexas sobre o Docker](https://docs.docker.com/engine/reference/run/), mas juro que tudo necessário está neste README.

*Obviamente, tire os parênteses e troque pelo valor real*
